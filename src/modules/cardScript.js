var card = (function (pageData){
    
    var element = document.getElementsByClassName('card')[0];
    
    var currentPage = element.children[0].classList[0];
    
    var nextPage;
    
    var request = function (pageName){
        
        var path = pageData[pageName].href;
        
        var req = new XMLHttpRequest();
        
        req.open('GET', path, true);
        
        req.send();
        
        return req;
    };

    var getNext = function (){
        
        if (nextPage !== currentPage) {
            
        	var req = request(nextPage);
        	
        	req.addEventListener('load', function (){
        	    
        	    switch (req.status) {
        	        
        	    	case 200:
                        element.innerHTML = req.responseText;
                        currentPage = nextPage;
        	    		break;
        	    	
        	    	case 404:
                        element.innerHTML = "404 Page Not Found";
                        currentPage = nextPage;
                        break;
                        
        	    	default:
        	    		break;
        	    		
        	    }
        	});
        }
    	
        element.classList.remove('off');
        element.classList.add('on');
    	
    };
    
    return {
        element: element,
        setNext: function (name){
            nextPage = name
        },
        getNext: getNext
    };
    
})(pageData);