(function (card){
    
    //switches between two classes. If neither exist, add the 'on' class
    var switchClassOnOff = function (element, onClassName, offClassName){
        if (element.classList.contains(onClassName)) {
        	element.classList.remove(onClassName);
        	element.classList.add(offClassName);
        }
        else {
            element.classList.remove(offClassName);
            element.classList.add(onClassName);
        }
        if (!(element.classList.contains(onClassName) || element.classList.contains(offClassName))) {
            element.classList.add(onClassName);
        }
    };
    
    
    //based on second class name on element, addEventListener

    var setEvents = function (element){
        var key = element.classList[1];
        
        switch (key) {
        case 'wrapper':
            window.onresize = function (){
                if (window.innerWidth > 1200 && !element.classList.contains('desktop')){
                    element.classList.add('desktop');
                    element.classList.remove('mobile');
                    element.innerHTML = '';
                    element.appendChild(nav.desktop);
                }
                if (window.innerWidth < 1200 && !element.classList.contains('mobile')) {
                    nav.mobile.getElementsByClassName('dropDown')[0].classList.remove('close');
                    nav.mobile.getElementsByClassName('dropDown')[0].classList.remove('open');
                    element.innerHTML = '';
                	   element.appendChild(nav.mobile);
                    element.classList.remove('desktop');
                    element.classList.add('mobile');
                }
            };
            break;
        
    	case 'goto':
    	
            element.addEventListener('click', function (e){
                
                if (window.innerWidth < 1200) {
            	    switchClassOnOff(document.getElementsByClassName('dropDown')[0],"open","close");
                }
            
                card.element.classList.remove('on');
                card.element.classList.add('off');
                
                card.setNext(element.innerHTML.toLowerCase());
            });
    		
    		break;
    		
        case 'name':
        
            element.addEventListener('click', function (){
    		    switchClassOnOff(document.getElementsByClassName('dropDown')[0],"open","close");
    		});
    		
    		break;
        
        case 'content':
        
            element.addEventListener('click', function (){
    		    var dropDown = document.getElementsByClassName('dropDown')[0];
                if (dropDown.classList.contains('open')) {
                    dropDown.classList.remove('open');
                    dropDown.classList.add('close');
                }
    		});
    		
    		break;
    		
        case 'card':
            
            element.addEventListener('animationend', function (){
                if (element.classList.contains('off')) {
                    card.getNext();
                }            
            });

            
            break;
    		
        default:
        
            return function (){
                console.log('create a case for this');  
            };
                
        }
    };
    
    var eventAttacher = function (element){
        if (element.classList.contains('event')) {
            setEvents(element);
        }
        if (element.hasChildNodes()) {
        	for (var child of element.children) {
                eventAttacher(child);
        	}
        }
    };
    
    window.onload = function (){
        eventAttacher(document.body);
        if (window.innerWidth > 1200) {
            var wrapper = document.getElementsByClassName('wrapper')[0];
            wrapper.classList.add('desktop');
            wrapper.innerHTML = '';
            wrapper.appendChild(nav.desktop);
        }
        eventAttacher(nav.desktop);
        eventAttacher(nav.mobile);
    };

    
})(card);