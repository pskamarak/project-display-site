var nav = (function (){
    
    var navMobile = document.getElementsByTagName("nav")[0];
    
    var navDesk = navMobile.cloneNode(navMobile);
    navDesk.removeChild(navDesk.getElementsByTagName('ul')[0]);
    
    //populating nav bar
    for (var key in pageData){
        var mob = document.createElement('li');
        var desk = document.createElement('a');
        
        mob.innerHTML = pageData[key].label;
        desk.innerHTML = pageData[key].label;
        
        mob.classList.add('event','goto');
        desk.classList.add('event','goto');
        
        navMobile.getElementsByTagName('ul')[0].appendChild(mob);
        navDesk.appendChild(desk);
    }
    
    navMobile = navMobile.cloneNode(true);
    
    return {
        mobile: navMobile,
        desktop: navDesk
    };
    
})();