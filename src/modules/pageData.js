var pageData = {
    "home":{
        label: "Home",
        href: "./html/default.html"
    },
    "about":{
        label: "About",
        href: "./html/about.html"
    },
    "portfolio":{
        label: "Portfolio",
        href: "./html/portfolio.html"
    },
    "contact":{
        label: "Contact",
        href: "./html/contact.html"
    }
};