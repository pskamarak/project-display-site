/**
 * Hello!
 * First of all I, am honored that you are interested enough to look at my code.
 * However, beyond you will find nothing but spaghetti. I created this application
 * as I was learning JS in my web application class, learning as I moved.
 * I encourage you to look at the code for my resume site. It is a much more accurate
 * representation of my work.
 */





//variables
var add,
    complete,
    completeList,
    help,
    view,
    viewList,
    c = ['view','add','complete','help','content','options','list'],
    currentTab,
    taskArr = [];
    
//class definitions
var Task = class Task{
    constructor(description, priority, completeBy, completed, dateCompleted){
        this.description = description;
        this.priority = priority;
        this.dateCreated = Date.now();
        this.completeOn = completeBy;
        this.dateCompleted = dateCompleted;
        this.completed = completed; 
    }
    toggleComplete(){
        if (this.completed === false){
            this.completed = true;
            this.dateCompleted = Date.now();
            return 1;
        }
        if (this.completed === true){
            this.completed = false;
            this.dateCompleted = null;
            return -1;
        }
    }
    
}

//functions related to the Task class and task elements
function populateTasks(taskArray){ 
    
    var k = Object.keys(taskArray);
    
    for (k in taskArray){
        insertTask(createTaskElement(taskArray[k],k));
        
    }
}

function createTaskElement(Task, key){ //creates and returns a task element
    var p = document.createElement("p");
    var div = document.createElement("div");
    
    div.classList.add('task',key.toString());
    
    p.appendChild(document.createTextNode(Task.description));
    
    div.appendChild(p);
    
    if (Task.completed === true){
        div.classList.add('complete');
    }
    if (Task.completed === false){
        div.classList.add('view');
        div.setAttribute('onclick','select(this);');
    }
    
    return div;
}


function addTask(){
    var i = 0; 
    if (document.getElementById('description').value !== '') {
        var t = new Task(document.getElementById('description').value,
        document.getElementById('priority').value,
        document.getElementById('dateToComplete').valueAsNumber + (14400000*4),
        false, null);
        while (taskArr[t.completeOn.toString() + i] != undefined){
            i++;        
        }
        var key = t.completeOn.toString() + i;
        taskArr[key] = t;
        
        insertTask(createTaskElement(taskArr[key],key));
        //saveTaskToLocal(key, t);
    }
}

function insertTask(taskElement){
    if (taskElement.classList.contains('complete')){
        completeList.appendChild(taskElement);
    }        
    if (!taskElement.classList.contains('complete')){
        viewList.appendChild(taskElement);
    }
}

function moveToComplete(){
    
    var selected = viewList.getElementsByClassName('selected');
    
    var keys = [];
    
    if (selected.length > 0){
        
        for (var task of selected){
            keys.push(task.classList[1]);
        }
        
        for (var key of keys){
            taskArr[key].completed = true;
            completeList.appendChild(viewList.getElementsByClassName(key)[0]);
            completeList.getElementsByClassName(key)[0].classList.remove('selected');
            
        }   
    }
}


function select(element){
    if (element.classList.contains('selected')){
        element.classList.remove('selected');
        return;
    }
    if (!element.classList.contains('selected')){
        element.classList.add('selected');
        return;
    }
}

function clearList(){
    viewList.innerHTML = '';
    completeList.innerHTML = '';
}

function selectAll(){
    for (var a of viewList.children){
        a.classList.add('selected');
    }
}

function deselectAll(){
    for (var a of viewList.children){
        a.classList.remove('selected');
    }
}


//initializing functions
document.onreadystatechange = function (){
    if (document.readyState === 'complete'){
        initTabs();
        startingTab(); 
        document.getElementById('dateToComplete').valueAsNumber = new Date();
    }
};
function initTabs(){
    loadViewTab();     //in view.js
    loadAddElements(); //in add.js
    loadCompleteTab(); //in complete.js
    loadHelpTab();     //in help.js
}

//functions relating to 'tabs'
function switchTo(element){
    var b = element.classList;
    var target = document.getElementById('here');
    switch(b[0]){
        case 'view':
            if (currentTab !== view){
                loadTab(view);
                target.classList.remove('complete', 'help');
                target.classList.add('view');
            }
            break;
        case 'complete':
            if (currentTab !== complete){
                loadTab(complete);  
                target.classList.remove('view','help');
                target.classList.add('complete')
            }
            break;
        case 'help':
            if (currentTab !== help){
                loadTab(help);
                target.classList.remove('view', 'complete');
                target.classList.add('help');
            }
            break;
    }
}

function loadTab(tab){
    var content = document.getElementById('here');
    turnButtonWhite(currentTab);
    content.removeChild(currentTab);
    content.appendChild(tab);
    currentTab = tab;
    turnButtonGrey(currentTab);
}

function startingTab(){ //in the future this will be customizable by the user
    var content = document.getElementById('here');
    content.appendChild(view);
    currentTab = view;
    turnButtonGrey(currentTab);
}

//add tab
function loadAddElements(){
    add = document.getElementsByClassName('add content')[0];
    document.getElementById('here').style.height = (window.innerHeight - 120).toString()+'px';
    add.getElementsByTagName('input')[1].disabled = true;
    add.getElementsByTagName('select')[0].disabled = true;
}

function addButtonOnClick(){
    var here = document.getElementById('here').style;
    var isOpen = add.classList.contains('open');
    if (isOpen === true){
        add.classList.remove('open');
        add.classList.add('closed');
        document.getElementById('here').classList.remove('open');
        here.height = (window.innerHeight - 120).toString()+'px';
    }
    if (isOpen === false){
        add.classList.remove('closed');
        add.classList.add('open');
        document.getElementById('here').classList.add('open');
        here.height = (parseInt(here.height.valueOf()) - 144).toString() + "px";
    }
}

function turnButtonGrey(tab){ //turn the button associated with the given tab grey
    var tabName = tab.classList[0];
    document.getElementsByClassName(tabName)[0].style.backgroundColor = 'lightgrey';
}

function turnButtonWhite(tab){ //turn the button assiciated with the given tab white
    var tabName = tab.classList[0];
    document.getElementsByClassName(tabName)[0].style.backgroundColor = 'white';
}

//complete tab
function loadCompleteTab(){
    complete = document.createElement('div');
    complete.classList.add(c[2],c[4]);
    
    completeList = document.createElement('div');
    completeList.classList.add(c[2],c[6]);
    completeList.style.height = (window.innerHeight-60).toString()+'px';
    completeList.style.top = '0px';
    
    complete.appendChild(completeList);
}

//help tab
function loadHelpTab(){
    var label1 = document.createElement('label'),
        label2 = document.createElement('label'),
        saveButton = document.createElement('button'),
        loadButton = document.createElement('button'),
        clearButton = document.createElement('button');
        
    help = document.createElement('div');
    help.classList.add(c[3],c[4]);
    
    label1.innerHTML = "Automatic saving of tasks is possible, but not implemented. Use the following buttons to save before you exit.<br>";
    saveButton.innerHTML = "Save";
    loadButton.innerHTML = "Load";
    clearButton.innerHTML = "Clear";
    
    saveButton.onclick = function(){
        var k = Object.keys(taskArr);
    
        for (k in taskArr){
            localStorage.setItem(k,JSON.stringify(taskArr[k]));
        }
    };
    loadButton.onclick = function(){
        for (var i = 0; i < localStorage.length; i++){
            var storedItem = JSON.parse(localStorage.getItem(localStorage.key(i)));
            var t = new Task(storedItem.description, storedItem.priority, storedItem.completeOn, storedItem.completed, storedItem.dateCompleted);
            taskArr[localStorage.key(i)] = t;
        }
        populateTasks(taskArr);
    };
    clearButton.onclick = function(){
        window.localStorage.clear();
    };
    help.appendChild(label1);
    help.appendChild(saveButton);
    help.appendChild(loadButton);
    help.appendChild(clearButton);
    
}

//view tab
function loadViewTab(){
    var options = document.createElement('div'),
        completeButton = document.createElement('button'),
        selectAllButton = document.createElement('button'),
        deselectAllButton = document.createElement('button');
    
    
    view = document.createElement('div');
    view.classList.add(c[0],c[4]);
    
    viewList = document.createElement('div');
    
    
    completeButton.innerHTML = 'Complete';
    completeButton.onclick = function(){
        moveToComplete();
    }
    selectAllButton.innerHTML = 'Select All';
    selectAllButton.onclick = function(){
        selectAll();
    }
    deselectAllButton.innerHTML = 'Deselect All';
    deselectAllButton.onclick = function(){
        deselectAll();
    }
    
    viewList.classList.add(c[0],c[6]);
    
    view.appendChild(viewList);
    view.appendChild(options);
    
    options.classList.add(c[0],c[5]);
    options.appendChild(completeButton);
    options.appendChild(selectAllButton);
    options.appendChild(deselectAllButton);
}

